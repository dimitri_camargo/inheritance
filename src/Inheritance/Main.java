/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inheritance;

import Imported.Person;
import java.util.Scanner;

/**
 *
 * @author aluno.redes
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //      A U L A  4
        //Dealing with generic objects
        Person luis = new Person();
        
        Person vinicius = new Students();
        
        luis.setName("luis");
        luis.setAge(18);
        luis.setHeight(1.70);
        luis.setWeight(70);        
//        System.out.println(luis.getInfo());
        
        vinicius.setName("vinicius");
        vinicius.setAge(28);
        vinicius.setHeight(1.90);
        vinicius.setWeight(95);
//        System.out.println(vinicius.getInfo());
        
        if (vinicius instanceof Students) {        
        //casting to student object
        ((Students)vinicius).setNote1(4.5);
        ((Students)vinicius).setNote2(8);
        ((Students)vinicius).setFrequency(75);
        ((Students)vinicius).setSR(2007059);
        
        System.out.println(((Students)vinicius).getInfoStudents()+"\n");
        }else{
            System.out.println(vinicius.getInfo());
        }
        
        if (luis instanceof Students) {
            System.out.println(((Students)luis).getInfoStudents());
        }else{
            System.out.println(luis.getInfo());
        }
        
        Person arr[] = new Person[2];
        arr[0] = luis;
        arr[1] = vinicius;
        for (int i = 0; i < arr.length; i++) {
            //COMEÇAR PELO MAIS ESPECÍFICO E
            if (arr[i] instanceof Students) {
                System.out.println("ARR_STUDENT->"+(i+1)+"\n"+((Students)arr[i]).getInfoStudents());                
            } //ACABAR PELO MAIS GENÉRICO
            else if(arr[i] instanceof Person){
                System.out.println("ARR_PERSON->"+(i+1)+"\n"+arr[i].getInfo());
            }
            
            System.out.println("ARR->"+(i+1)+"\n"+arr[i].getInfo());
        }
        
        
        // AULAS ANTERIORRES
        /*Students obj1 = new Students("jose", 2003045, 100, 8.5);
        
        obj1.setName("josé");
        obj1.setAge(25);
        obj1.setHeight(1.80);
        obj1.setWeight(82);
        System.out.println("obj1"+obj1.getInfoStudents());*/                
        
/*        Scanner sc = new Scanner(System.in);
        System.out.println("how many students do you want?: ");
        int amount = sc.nextInt();
        Students arrObj[] = new Students[amount];
        
        for (int i = 0; i < arrObj.length; i++) {
            Students obj = new Students();
            System.out.println("set the name of the "+(i+1)+"° student: ");
            obj.setName(sc.next());
            
            System.out.println("set the SR of the "+(i+1)+"° student: ");
            obj.setSR(sc.nextInt());
            
            System.out.println("set the first note of the "+(i+1)+"° student: ");
            obj.setNote1(sc.nextDouble());
            
            System.out.println("set the second note of the "+(i+1)+"° student: ");
            obj.setNote2(sc.nextDouble());
            
            System.out.println("set the frequency of the "+(i+1)+"° student: ");
            obj.setFrequency(sc.nextInt());
            System.out.println("----------------------------------------------------");
            arrObj[i] = obj;
        }
        
        for (int i = 0; i < arrObj.length; i++) {
            System.out.println("info of the "+(i+1)+"° student: "+arrObj[i].getInfoStudents());
            System.out.println("is the "+(i+1)+"° student approved? "+arrObj[i].isApproved()+"\n");
        }                                                                                                   */
    }
    
}
