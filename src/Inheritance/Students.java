/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inheritance;

import Imported.Person;

/**
 *
 * @author aluno.redes
 */
public class Students extends Person{
    //attributes
    private int SR;
    private double note1;
    private double note2;
    private int frequency; 
    
    //constructor
    public Students(){
        System.out.println("students constructor");
    }
    
    //constructor 2 overloading 
    public Students(String name, int SR, double note1, double note2){
        //from superclass Person
        super.setName(name);//--------------------------------------------------------------------------
        //from this class
        setSR(SR);
        setNote1(note1);
        setNote2(note2);
    }
    
    
    //getters and setters
    public int getSR() {
        return SR;
    }

    public void setSR(int SR) {
        this.SR = SR;
    }

    public double getNote1() {
        return note1;
    }

    public void setNote1(double note1) {
        if (note1 < 0) {
            System.out.println("note can`t be negative!");
        } else {
            this.note1 = note1;
        }        
    }

    public double getNote2() {
        return note2;
    }

    public void setNote2(double note2) {
        if (note2 < 0) {
            System.out.println("note can`t be negative!");
        } else {
            this.note2 = note2;
        }            
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        if (frequency >= 0 ) {
            this.frequency = frequency;
        } else {
            System.out.println("frequency must be >= 0");
        }
        
    }
    
    public boolean isApproved(){
        if (((this.note1 + this.note2) / 2 >= 6)    &&    (this.frequency >= 75)){
            return true;
        } else {
            return false;
        }
    }
    
    public String getInfoStudents(){
        return "*********STUDENT\nname: "+super.name+"\nSR: "+this.SR+"\nnote1: "+this.note1+"\nnote2: "
                +this.note2+"\nfrequency: "+this.frequency+"\naverage: "+(this.note1+this.note2)/2;
    }
    
}
